from sqlalchemy import Column, Integer, String, ForeignKey, Date
from sqlalchemy.orm import relationship
from flask_appbuilder import Model

class Message(Model):
    id = Column(Integer, primary_key=True)
    category = Column(String(255), unique = True, nullable=False)
    message = Column(TEXT, nullable=False)

    def __repr__(self):
        return self.category