import json
import pytz
from datetime import datetime
from utils import (
    create_request_and_return_soup,
    parse_table_by_id,
    process_result,
    parse_date,
    format_and_clean_json,
    ajax_parse_data,
)


def scrap_jugando_online():
    """
    returns: json data of results
    """
    soup = create_request_and_return_soup(scrap_url="https://www.jugandoonline.com.ar/")
    table_data = parse_table_by_id(soup=soup, tableid="ContentPlaceHolder1_GridViewTop")
    result_data, expand_data = process_result(table_data)
    # print(json.dumps(result_data, indent=4))
    return result_data


def scrap_vivitusuerte():
    """
    return json data of results
    """
    date_soup = create_request_and_return_soup(
        scrap_url="http://www.vivitusuerte.com/busqfecha.php"
    )
    date = parse_date(date_soup)
    print(date)
    body_soup = create_request_and_return_soup(
        scrap_url="http://www.vivitusuerte.com/datospizarra.php?fecha=%s" % date
    )

    table_data = parse_table_by_id(soup=body_soup, tableid=None)
    ajax_data = ajax_parse_data(table_data, date)

    result_data, expand_data = process_result(table_data)
    print(len(result_data), "\n\n", len(expand_data))
    del expand_data[0]
    for elem in result_data:
        elem["expand"] = expand_data[result_data.index(elem)]
    # print(json.dumps(result_data, indent=4))
    # print(result_data)
    return result_data


def main():
    """scraps data"""
    jugando_data = scrap_jugando_online()
    vivitusuerte_data = scrap_vivitusuerte()
    jugando_data.extend(vivitusuerte_data)
    jugando_data = format_and_clean_json(jugando_data)
    filename = "data/data-%s.json" % (
        datetime.now(pytz.timezone("America/Argentina/Buenos_Aires")).strftime(
            "%Y-%m-%d"
        )
    )
    if jugando_data:
        with open(filename, "w") as f:
            json.dump(jugando_data, f)
        with open("data.json", "w") as fp:
            json.dump(jugando_data, fp)


if __name__ == "__main__":
    main()
    # scrap_vivitusuerte()

