import requests
from bs4 import BeautifulSoup
from urllib3 import get_host
import json

headers = {
    "authority": "www.jugandoonline.com.ar",
    "pragma": "no-cache",
    "cache-control": "no-cache",
    "dnt": "1",
    "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36",
    "sec-fetch-user": "?1",
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "sec-fetch-site": "none",
    "sec-fetch-mode": "navigate",
    "accept-encoding": "gzip, deflate, br",
}


def create_request_and_return_soup(scrap_url):

    headers["authority"] = get_host(scrap_url)[1]
    response = requests.get(scrap_url, headers=headers)
    html_soup = BeautifulSoup(response.text, "html.parser")
    return html_soup


def parse_table_by_id(soup, tableid):
    if tableid:
        valid_table = soup.find("table", id=tableid)
    else:
        valid_table = soup.find("table")
    rows = valid_table.find_all("tr")

    headers = {}
    thead = valid_table.find("thead")
    table_headers = [
        elem.text.strip().replace("La Primera", "Primera")
        for elem in valid_table.find_all("th")
    ]
    # print(table_headers)
    if thead:
        thead = thead.find_all("th")
        for i in range(len(thead)):
            headers[i] = thead[i].text.strip()
    data = [table_headers]
    for row in rows:
        cells = row.find_all("td")
        if thead:
            items = {}
            for index in headers:
                items[headers[index]] = cells[index].text
        else:
            items = []

            for index in cells:

                if index.text.strip().startswith("-"):
                    items.append(index.text.strip())
                elif index.find_all(
                    "input", attrs={"type": "hidden", "class": "idloteria"}
                ):
                    # print(index)
                    tdsoup = BeautifulSoup(str(index), "html.parser")
                    value = tdsoup.find("input").get("value")
                    items.append(value)
                else:
                    new_text = index.text.strip().replace("Prov Bs As", "Provincia")
                    if new_text.endswith("rdoba"):
                        new_text = "Cordoba"
                    elif new_text == "Entre Ríos":
                        new_text = "Entre Rios"
                    items.append(new_text)
        if items:
            data.append(items)
    return data


def clean_data(data):
    expand_data = []
    if len(data[0]) >= 6:
        new_data = []
        for elem in data:
            # print(elem)
            expand_data.append(elem[1])
            del elem[1]
            new_data.append(elem)
        return [elem for elem in new_data if elem], expand_data
    else:
        return [elem for elem in data], expand_data


def process_result(data):

    new_data = [elements for elements in data if elements]
    data, expand_data = clean_data(new_data)
    result = [
        {
            "name": elem[0],
            "values": {
                data[0][1]: {"value": elem[1] if len(elem[1])<=4 else '--'},
                data[0][2]: {"value": elem[2] if len(elem[2])<=4 else '--'},
                data[0][3]: {"value": elem[3] if len(elem[3])<=4 else '--'},
                data[0][4]: {"value": elem[4] if len(elem[4])<=4 else '--'},
            },
        }
        for elem in data[1:]
    ]

    return result, expand_data


def parse_date(soup):
    date_list = soup.find("input")["value"].split("/")
    date = "/".join(
        ["0%s" % elem if int(elem) < 10 else elem for elem in date_list][::-1]
    )
    return date


def format_and_clean_json(raw_data):
    
    name = []
    result = []
    for elem in raw_data:
        if elem["name"] not in name and elem.get("expand", ""):
            name.append(elem["name"])
            result.append(elem)
        # elif elem["name"] not in name and not elem.get("expand", ""):
        #     name.append(elem["name"])
        #     result.append(elem)
    for elem in raw_data:
        if elem["name"] not in name:
            name.append(elem["name"])
            result.append(elem)

    return [e for e in result]


def parse_recursive_table(body_soup):
    rtable_soup = body_soup.find_all("table", attrs={"align": "center"})
    # print(rtable_soup[0])
    rclean_table_data = []
    for rtable in rtable_soup:
        rtable_data = parse_table_by_id(BeautifulSoup(str(rtable), "html.parser"), None)
        if len(rtable_data) > 2:
            rclean_table_data.append(rtable_data)
    data = []

    for elem in rclean_table_data:
        name = elem[1][0]
        avalues = []
        inside_values = {}
        for relemai in elem[2:]:

            if len(relemai) == 4:
                inside_values[relemai[0].replace(".", "")] = relemai[1]
                inside_values[relemai[2].replace(".", "")] = relemai[
                    3
                ]  # {relemai[0]: , relemai[2]: relemai[3]}
            avalues.append(inside_values)

        data.append({"name": name, "values": inside_values})
    return data


def ajax_parse_data(table_data, date):
    return_data = []
    for elem in table_data[2:]:
        body_soup = create_request_and_return_soup(
            scrap_url="http://www.vivitusuerte.com/datospizarra_loteria.php?fecha=%s&loteria=%s"
            % (date, elem[6])
        )
        recursive_data = parse_recursive_table(body_soup)
        elem[1] = recursive_data
        return_data.append(elem[:6])

    return return_data




