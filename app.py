from flask import Flask, jsonify, request, render_template, redirect
import sqlite3
from flask import g
from datetime import datetime
import json
import os
# from sqlalchemy import text


app = Flask(__name__)

dir_path = os.getcwd()
database = dir_path + '/db.sqlite'

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(database)
        return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.after_request
def add_headers(response):
    white_origin = ['https://www.quinielashoy.com', 'https://staging.quiniela.workers.dev',
                    'https://quiniela.quiniela.workers.dev', 'http://api.quinielashoy.com', 'http://localhost:8000',
                    'http://localhost:28100']

    if 'HTTP_ORIGIN' in request.environ and request.environ['HTTP_ORIGIN'] in white_origin:
        response.headers.add('Content-Type', 'application/json')
        response.headers.add('Access-Control-Allow-Origin', request.headers['Origin'])
        response.headers.add('Access-Control-Allow-Methods', 'GET')
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
        response.headers.add('Access-Control-Expose-Headers', 'Content-Type,Content-Length,Authorization,X-Pagination')
    return response


@app.route('/')
def index():
    date = request.args.get('date')
    data = ''
    if date:
        requested_date = datetime.strptime(date, '%Y-%m-%d')
        filename = 'data/data-%s.json' % (requested_date.strftime("%Y-%m-%d"))
    else:
        filename = 'data/data-%s.json' % (datetime.now().strftime("%Y-%m-%d"))
        if not os.path.exists(filename):
            filename = 'data.json'

    try:
        with open(filename, 'r') as fp:
            data = json.load(fp)
    except Exception as e:
        print(e)

    if data:
        return jsonify(data)
    else:
        return {'error ': 'request in a while'}


@app.route('/notify')
def hello():
    db = get_db()
    # sql = text('select * from messages')
    messages = db.execute('select * from messages')
    message_obj = {}
    for message in messages:
        message_obj[message[1]] = message[2]
    print(message_obj)
    return render_template('notification_form.html', data=message_obj)


@app.route('/save_notify', methods=['POST'])
def save_notify():
    primeria = request.form['primeria']
    matutina = request.form['matutina']
    vespertina = request.form['vespertina']
    nocturna = request.form['nocturna']

    db = get_db()
    print('db connection is ', db)
    sqliteConnection = sqlite3.connect(database)
    cursor = sqliteConnection.cursor()
    cursor.execute("""UPDATE messages SET message = ? WHERE category = 'Primeria'""", (primeria,))
    cursor.execute("""UPDATE messages SET message = ? WHERE category = 'Matutina'""", (matutina,))
    cursor.execute("""UPDATE messages SET message = ? WHERE category = 'Vespertina'""", (vespertina,))
    cursor.execute("""UPDATE messages SET message = ? WHERE category = 'Nocturna'""", (nocturna,))
    sqliteConnection.commit()

    return redirect('/notify')


@app.route('/subscribe', methods=['POST'])
def save_subscription():
    print(json.loads(request.data.decode()))
    token = request.data.get('token')
    categories = request.data.get('categories')
    print(categories)
    sqliteconnection = sqlite3.connect(database)
    cursor = sqliteconnection.cursor()

    # check device id exist else create new one
    cursor.execute("""select * from devices WHERE token= ?""", (token,))
    result = cursor.fetchone()
    if cursor:
        device_id = result[0]
    else:
        cursor.execute("INSERT INTO devices VALUES (?)", (token,))
        sqliteconnection.commit()
        cursor.execute("""select * from devices WHERE token= ?""", (token,))
        result = cursor.fetchone()
        device_id = result[0]

    print(device_id)
    # subscribe device for notifications
    cursor.execute("""select * from messages WHERE category IN ?""", (",".join(map(str, categories)),))
    categories = cursor.fetchall()

    print(categories)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=28100, debug=True)
