class Notification:
    """
    A Notification class to store notification messages
    """

    def __init__(self, message, category):
        self.message = message
        self.category = category

    def __repr__(self):
        return "Notification Message"